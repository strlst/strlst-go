package main

import (
	"fmt"
	"os"
	"strings"
	"regexp"
	"bufio"
	"strconv"
	"path/filepath"
)

const (
	CONFIG_DIRECTORY_DEFAULT string = "~/.config/strlst/"
	CONFIG_FILENAME_DEFAULT string = "strlst"
	CONFIG_EXAMPLE string = `# this is an example configuration with a semantic placeholder in each field
# lines beginning with a number sign are comments
# accessible ip or domain of webserver
url=somewhere.someplace/somesubdomain
# whether to use https
https=true
# whether to allow write capabilities (store, push, commit)
readonly=false
# path to the identity file for automatic transfer
ssh_identity_path=~/.ssh/copyservice_rsa
# ssh git remote repository url and branch
ssh_repository_url=git@example.com:user/repository.git
ssh_repository_branch=pages
# path to the local repository to commit changes to
repository_path=~/path/to/repo
# secret for encryption, keep this safe!
secret=examplesecrettobekeptsafe
`
)

type Configuration struct {
	// specifies where to look in the form of domain[/subdomain[/subdomains]]
	url string
        // whether to use tls
        https bool
	// whether to allow write capabilities
        readonly bool
        // ssh identity to push to repository
	ssh_identity_path string
        // ssh git remote repository url and branch
	ssh_repository_url string
	ssh_repository_branch string
        // where files are stored
	repository_path string
	// specifies encryption secret
        // keep this secret!
	secret string
}

func GetHomeDir() string {
	// query home directory
	home, err := os.UserHomeDir()
	if err != nil {
		fmt.Fprintf(os.Stderr, "fatal error: user home directory could not be determined\n")
		os.Exit(EXIT_HOME_INVALID)
	}

	return home
}

func GetConfigDefaultPath() string {
	return filepath.Join(GetHomeDir(), ".config", "strlst", "strlst")
}

func ConfigurationToString(config *Configuration, obfuscate_secret bool) string {
	secret := "***"
	if !obfuscate_secret {
		secret = config.secret
	}

	return fmt.Sprintf(
		"configuration { url = %s, https = %t, readonly = %t, ssh_identity_path = %s, ssh_repository_url = %s, ssh_repository_branch = %s, repository_path = %s, secret = %s }",
		config.url,
		config.https,
		config.readonly,
		config.ssh_identity_path,
		config.ssh_repository_url,
		config.ssh_repository_branch,
		config.repository_path,
		secret,
	)
}

func GetConfiguration(program *string, path *string) Configuration {
	config := Configuration {}

	// open file for reading
	configuration_file, err := os.Open(*path)
	// if file does not exist
	if os.IsNotExist(err) {
		fmt.Fprintf(os.Stderr, "%s: specified configuration file does not exist\n", *program)
		os.Exit(EXIT_SPECIFIED_PATH_INVALID)
	}
	defer configuration_file.Close()

	// process configuration file line for line
	matchers := [...](*regexp.Regexp) {
		regexp.MustCompile("^#.*$"),
		regexp.MustCompile("^url *= *([^ ].*)$"),
		regexp.MustCompile("^https *= *((true|.*))$"),
		regexp.MustCompile("^readonly *= *((true|false))$"),
		regexp.MustCompile("^ssh_identity_path *= *([^ ].*)$"),
		regexp.MustCompile("^ssh_repository_url *= *([^ ].*)$"),
		regexp.MustCompile("^ssh_repository_branch *= *([^ ].*)$"),
		regexp.MustCompile("^repository_path *= *([^ ].*)$"),
		regexp.MustCompile("^secret *= *([^ ].*)$"),
	}

	// create scanner to iterate file contents
	scanner := bufio.NewScanner(configuration_file)
	// iterate file contents
	for scanner.Scan() {
		line := scanner.Text()

		// match line against matchers to parse contents
		for index, matcher := range matchers {
			if matcher.MatchString(line) {
				// comment, skip
				if index == 0 { continue }

				// find submatch (singular) for anything that isn't a comment
				submatches := matchers[index].FindAllStringSubmatch(line, 1)
				if len(submatches) < 1 || len(submatches[0]) < 2 {
					fmt.Fprintf(os.Stderr, "%s: error matching at line: %s\n", *program, line)
					continue
				}
				submatch := submatches[0][1]

				// ugly solution, maybe use maps instead
				switch index {
				case 1: config.url = submatch
				case 2:
					value, err := strconv.ParseBool(submatch)
					if err != nil {
						fmt.Fprintf(os.Stderr, "%s: error parsing https %s\n", *program, submatch)
						os.Exit(EXIT_CONFIG_INVALID)
					}
					config.https = value
				case 3:
					value, err := strconv.ParseBool(submatch)
					if err != nil {
						fmt.Fprintf(os.Stderr, "%s: error parsing readonly %s\n", *program, submatch)
						os.Exit(EXIT_CONFIG_INVALID)
					}
					config.readonly = value
				case 4: config.ssh_identity_path = submatch
				case 5: config.ssh_repository_url = submatch
				case 6: config.ssh_repository_branch = submatch
				case 7: config.repository_path = submatch
				case 8: config.secret = submatch
				}
				break
			}
		}
	}

	return config
}

func IsConfigurationValid(config *Configuration) bool {
	// very basic validation
	return strings.Contains(config.url, ".") &&
		(config.readonly || strings.Contains(config.ssh_identity_path, "/")) &&
		(config.readonly || strings.Contains(config.ssh_repository_url, "/")) &&
		(config.readonly || len(config.ssh_repository_branch) > 0) &&
		(config.readonly || strings.Contains(config.repository_path, "/")) &&
		config.secret != ""
}
