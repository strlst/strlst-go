CXX:=go
GOBUILD:=$(CXX) build
GOCLEAN:=$(CXX) clean

PROG:= strlst
SOURCES:= strlst.go config.go commands.go

all: build run

build:
	$(GOBUILD) $(SOURCES)

release:
	$(GOBUILD) -ldflags="-s -w" $(SOURCES)

install: release
	cp $(PROG) /usr/local/bin

uninstall:
	rm -f /usr/local/bin/$(PROG)

run:
	./$(PROG)

clean:
	$(GOCLEAN)

test:
	./$(PROG) list
