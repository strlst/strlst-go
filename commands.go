package main

import (
	"fmt"
	"strings"
	"sync"
	"os"
	"os/exec"
	"os/user"
	"path/filepath"
	"time"
)

func ConstructUrl(config *Configuration) string {
	if config.https {
		return fmt.Sprintf("https://%s", config.url)
	} else {
		return fmt.Sprintf("https://%s", config.url)
	}
}

func AsPathStr(path string) string {
	if strings.HasPrefix(path, "~/") {
		usr, _ := user.Current()
		dir := usr.HomeDir
		path = filepath.Join(dir, path[2:])
	}
	return path
}

func RunCmd(cmd *exec.Cmd) error {
	// relay git messages to user
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}

func LocalRepositoryDoesNotExist(config *Configuration, create_directory bool) bool {
	// stat repository path
	repository_path := AsPathStr(config.repository_path)
	_, err := os.Stat(repository_path)

	// it exists
	if err == nil {
		return true
	}

	// it doesnt exists and we are supposed to create it
	if os.IsNotExist(err) && create_directory {
		fmt.Printf("local repository directory %s does not exist, will be created\n", repository_path)

		// construct clone command and run
		cmd_git_clone := exec.Command("git", "clone", config.ssh_repository_url, repository_path)
		err := RunCmd(cmd_git_clone)
		if err != nil {
			os.Exit(EXIT_CLONE_FAILED)
		}

		// construct fetch command and run
		cmd_git_fetch := exec.Command("git", "fetch", "--all")
		cmd_git_fetch.Dir = repository_path
		RunCmd(cmd_git_fetch)

		// construct checkout command and run
		cmd_git_checkout := exec.Command("git", "checkout", "-b", config.ssh_repository_branch, fmt.Sprintf("origin/%s", config.ssh_repository_branch))
		cmd_git_checkout.Dir = repository_path
		err = RunCmd(cmd_git_checkout)
		if err != nil {
			os.Exit(EXIT_CHECKOUT_FAILED)
		}

		return true
	}
	return false
}

func CreateIndex(config *Configuration, placeholder1 []string, placeholder2 bool) {
	repository_path := AsPathStr(config.repository_path)
	index := fmt.Sprintf("%s/index", repository_path)

	// ls | cat
	cmd_ls := exec.Command("ls")
	cmd_ls.Dir = repository_path
	cmd_cat := exec.Command("cat")

	// pipe ls into cat
	pipe_ls_to_cat, err := cmd_ls.StdoutPipe()
	if err != nil {
		fmt.Fprintf(os.Stderr, "fatal error: construction of pipe failed\n")
		// this error is fatal
		return
	}

	// make sure to close pipe at the end (if it was opened)
	defer pipe_ls_to_cat.Close()

	// finalize pipe
	cmd_cat.Stdin = pipe_ls_to_cat

	// create file in preparation to execute command
	file, err := os.Create(index)
	defer file.Close()
	// handle error
	if err != nil {
		fmt.Fprintf(os.Stderr, "error: cannot create file for index at %s: %v\n", index)
		os.Exit(EXIT_INDEX_NOT_CREATABLE)
	}

	// assign stdout to newly created file
	cmd_cat.Stdout = file

	// let both initial commands do their work
	cmd_ls.Start()

	// finally let the actual command do it's work
	cmd_cat.Run()

	// finally query stdout/stderr of last command
	if err != nil {
		fmt.Fprintf(os.Stderr, "index could not be created\n")
	} else {
		fmt.Printf("successfully created index\n")
	}
}

func CloneRemoteRepository(config *Configuration, placeholder1 []string, placeholder2 bool) {
	exists := LocalRepositoryDoesNotExist(config, false)
	if exists {
		fmt.Printf("repository already exists at %s\n", AsPathStr(config.repository_path))
	} else {
		// just call function again with flag
		LocalRepositoryDoesNotExist(config, true)
		fmt.Printf("successfully cloned %s to %s\n", config.ssh_repository_url, AsPathStr(config.repository_path))
	}
}

func ListRemoteFiles(config *Configuration, filenames []string, placeholder bool) {
	// define url of index
	url := fmt.Sprintf("%s/index", ConstructUrl(config))

	// define commands
	cmd_curl := exec.Command("curl", "-s", url)
	RunCmd(cmd_curl)
}

func PullRemoteFiles(config *Configuration, filenames []string, save bool) {
	// require at least one file
	if len(filenames) <= 0 {
		fmt.Printf("pull command requires at least one file\n")
		return
	}

	// NOTE: this function does allow duplicate filenames, which, quite magically, still works

	// define url
	url := fmt.Sprintf("%s/", ConstructUrl(config))

	// set up saving of files (instead of stdout output)
	single_file := len(filenames) <= 1
	save_files := save || !single_file

	// configure wait group for synchronization
	wg := sync.WaitGroup{}
	wg.Add(len(filenames))

	// iterate all filenames for pull action
	for _, f := range filenames {
		// skip problemativ filenames
		if strings.Contains(f, "/") {
			fmt.Printf("directories are not supported at the time\n")
			// finish task pre-emptively
			wg.Done()
			continue
		}

		// use goroutines because tasks are independent
		go func(wg *sync.WaitGroup, f string) {
			// finish task at the end
			defer wg.Done()

			//curl -s "${url}${1}" | openssl enc -d -aes-256-cbc -pbkdf2 --iter 32768 -nosalt -base64 -k "${secret}" > "${file}"
			cmd_curl := exec.Command("curl", "-s", fmt.Sprintf("%s%s", url, f))
			cmd_openssl := exec.Command("openssl", "enc", "-d", "-aes-256-cbc", "-pbkdf2", "-iter", "32768", "-nosalt", "-base64", "-k", config.secret)

			pipe_curl_to_openssl, err := cmd_curl.StdoutPipe()
			// pipe curl into grep
			if err != nil {
				fmt.Fprintf(os.Stderr, "fatal error: construction of pipe failed\n")
				// this error is fatal
				return
			}

			// make sure to close pipe at the end (if it was opened)
			defer pipe_curl_to_openssl.Close()
			// finalize pipe
			cmd_openssl.Stdin = pipe_curl_to_openssl

			// set up command output
			cmd_openssl.Stdout = os.Stdout
			if save_files {
				// create file in preparation to execute command
				file, err := os.Create(f)
				defer file.Close()
				// handle error if no such file exists exists on remote host
				if err != nil {
					fmt.Fprintf(os.Stderr, "cannot create file %s: %s\n", f, err.Error())
				}

				// assign stdout to newly created file
				cmd_openssl.Stdout = file
			}

			// let both initial commands do their work
			cmd_curl.Start()

			// finally let the actual command do it's work
			cmd_openssl.Run()

			// finally query stdout/stderr of last command
			if err != nil {
				fmt.Fprintf(os.Stderr, "file %s could not be pulled\n", f)
				return
			} else {
				if save_files {
					fmt.Printf("successfully pulled %s\n", f)
				}
			}
		}(&wg, f)
	}

	// synchronize (program has to wait for goroutines otherwise output will be lost)
	wg.Wait()
}

func CommitRemoteFiles(config *Configuration, placeholder1 []string, placeholder2 bool) {
	fmt.Sprintf("committing changes\n")

	// construct commit message
	commit_message := fmt.Sprintf("automated commit %s", time.Now().Format(time.RFC3339));

	// parse filepath
	repository_path := AsPathStr(config.repository_path)

	// construct add command
	cmd_git_add := exec.Command("git", "add", "*")
	// use repository path as execution directory
	cmd_git_add.Dir = repository_path
	// run command
	RunCmd(cmd_git_add)

	// construct commit command
	cmd_git_commit := exec.Command("git", "commit", "-m", commit_message)
	// use repository path as execution directory
	cmd_git_commit.Dir = repository_path
	// run command
	RunCmd(cmd_git_commit)

	// construct push command
	cmd_git_push := exec.Command("git", "push")

	// use repository path as execution directory
	cmd_git_push.Dir = repository_path
	cmd_git_push.Env = append(
		os.Environ(),
		fmt.Sprintf("GIT_SSH_COMMAND=ssh -i %s", config.ssh_identity_path),
	)
	// run command
	err := RunCmd(cmd_git_push)

	if err != nil {
		fmt.Fprintf(os.Stderr, "error: could not execute `git push` command: %v\n", err)
		os.Exit(EXIT_PUSH_FAILED)
	}

	fmt.Printf("successfully committed changes\n")
}

func StoreFilesLocal(config *Configuration, filenames []string, placeholder bool) {
	// require at least one file
	if len(filenames) <= 0 {
		fmt.Printf("pull command requires at least one file\n")
		return
	}

	// check for existence of local repository directory
	LocalRepositoryDoesNotExist(config, true)

	// parse filepath
	repository_path := AsPathStr(config.repository_path)

	// configure wait group for synchronization
	wg := sync.WaitGroup{}
	wg.Add(len(filenames))

	// iterate all filenames for pull action
	for _, f := range filenames {
		// skip problemativ filenames
		if strings.HasSuffix(f, "/") {
			fmt.Printf("directories are not supported at the time\n")
			// finish task pre-emptively
			wg.Done()
			continue
		}

		// use goroutines because tasks are independent
		go func(wg *sync.WaitGroup, f string) {
			// finish task at the end
			defer wg.Done()
			// because this is a naive reimplementation I'm not going to use the actual libraries here
			//openssl enc -e -aes-256-cbc -pbkdf2 --iter 32768 -nosalt -base64 -k "${secret}" -in "${1}" -out "${2}"

			// get absolute path of file
			file, err := filepath.Abs(f)
			if err != nil {
				fmt.Fprintf(os.Stderr, "error: could not get absolute path for %s, skipping...", f)
			}

			// get base and join to target directory
			file_out := filepath.Join(repository_path, filepath.Base(file))

			// define openssl command
			cmd_openssl := exec.Command(
				// openssl command with parameters
				"openssl", "enc", "-e", "-aes-256-cbc", "-pbkdf2", "-iter", "32768", "-nosalt", "-base64",
				// specify encryption secret
				"-k", config.secret,
				// specify input file
				"-in", file,
				// specify output file
				"-out", file_out,
			)

			// start openssl command
			err = cmd_openssl.Start()
			if err != nil {
				fmt.Fprintf(os.Stderr, "error: starting openssl command while trying to push %s: %v, skipping...\n", f)
				return
			}

			// wait and get exit code
			err = cmd_openssl.Wait()
			if err != nil {
				fmt.Fprintf(os.Stderr, "error: executing openssl command while trying to push %s: %v, skipping...\n", f, err)
				return
			} else {
				fmt.Printf("successfully stored %s\n", f)
			}
		}(&wg, f)
	}

	// synchronize (program has to wait for goroutines otherwise output will be lost)
	wg.Wait()

	// at the end construct index
	CreateIndex(config, filenames, placeholder)
}

func PushLocalFiles(config *Configuration, filenames []string, placeholder bool) {
	// a push consists of a store plus a commit
	StoreFilesLocal(config, filenames, placeholder)
	println()
	CommitRemoteFiles(config, filenames, placeholder)
}

// technically the map could be modified at runtime, making for exciting possibilities
type fn func (*Configuration, []string, bool)
var COMMANDS map[string]fn = map[string]fn {
	"index": CreateIndex,
	"clone": CloneRemoteRepository,
	"list": ListRemoteFiles,
	"pull": PullRemoteFiles,
	"commit": CommitRemoteFiles,
	"store": StoreFilesLocal,
	"push": PushLocalFiles,
}

func IsImplemented(command string) bool {
	if _, ok := COMMANDS[command]; ok {
		return true;
	} else {
		return false;
	}
}

func Exec(command string, config *Configuration, filenames []string, save bool) {
	COMMANDS[command](config, filenames, save)
}
